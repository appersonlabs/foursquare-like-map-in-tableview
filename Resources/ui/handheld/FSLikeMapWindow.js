function FSLikeMapWindow(title) {
	var self = Ti.UI.createWindow({
		title : title,
		backgroundColor : 'white'
	});

	var tableview = Ti.UI.createTableView({

	});

	self.add(tableview);
	var data = [];

	//fill the rows
	for (var i = 0; i < 21; i++) {
		data.push(Ti.UI.createTableViewRow({
			title : "Row " + i
		}));
	};

	tableview.setData(data);

	var mapSectionHeight = 100;
	//container to map
	var mapContainer = Ti.UI.createView({

	});

	var mapview = Titanium.Map.createView({
		animate : true,
		userLocation : true,
		zIndex : 0,
		top : 0,
		region : {
			latitude : 51.5,
			longitude : 0,
			latitudeDelta : 0.03,
			longitudeDelta : 0.03
		}

	});

	mapContainer.add(mapview);

	var mapOverlay = Ti.UI.createView({
		zIndex : 2
	});
	mapContainer.add(mapOverlay);

	//add the mapContainer to headerPullView
	tableview.headerPullView = mapContainer;

	//set insets
	tableview.setContentInsets({
		top : mapSectionHeight
	}, {
		animated : true
	});

	//go to top
	tableview.scrollToTop(-mapSectionHeight, {
		animated : true
	});

	var back = Ti.UI.createButton({
		title : "back",
		width : 120,
		top : 20,
		zIndex : 3
	});

	//EVENTS
	mapOverlay.addEventListener("click", function() {
		Ti.API.info("map overlay clicked");

		//add exact height
		mapContainer.height = mapSectionHeight;
		//get out from container and add to window
		self.add(mapContainer);
		//set the top
		mapContainer.top = 0;
		//animate to fullscreen
		//set the height of window inside
		mapContainer.animate({
			height : Ti.Platform.displayCaps.platformHeight - 110,
			duration : 300
		});

		mapContainer.add(back);
		mapOverlay.opacity = 0;

	});

	back.addEventListener("click", function() {
		//reverse anim

		mapContainer.animate({
			//110 - statusbar, navbar and tabheight.. approx..
			top : -(Ti.Platform.displayCaps.platformHeight - 110 - mapSectionHeight),
			duration : 300
		}, function() {
			//set back
			tableview.headerPullView = mapContainer;
			mapOverlay.opacity = 1;
		});

	});

	return self;
};

module.exports = FSLikeMapWindow;
