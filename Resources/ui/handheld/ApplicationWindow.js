function ApplicationWindow(title) {
	var self = Ti.UI.createWindow({
		title : title,
		backgroundColor : 'white'
	});

	/*
	 var pins = [];
	 pins.push(Ti.Map.createAnnotation({
	 animate : true,
	 latitude : 51.5,
	 longitude : 0,
	 pincolor : Ti.Map.ANNOTATION_RED,
	 subtitle : 'Mountain View, CA',
	 title : 'Appcelerator Headquarters'
	 }));

	 var mapview = Titanium.Map.createView({
	 animate : true,
	 userLocation : true,

	 regionFit : false,
	 annotations : pins

	 });
	 mapview.addEventListener("regionChanged", function() {

	 });

	 var region = {
	 latitude : 51.5,
	 latitudeDelta : .1,
	 longitude : 0.1,
	 longitudeDelta : .1,
	 };

	 Ti.API.debug(region);
	 mapview.startLayout();
	 mapview.setRegionFit(true);
	 mapview.setRegion(region);
	 mapview.currentRegion = region;
	 mapview.finishLayout();

	 self.add(mapview); */
	return self;
};

module.exports = ApplicationWindow;
